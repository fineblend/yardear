<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
class Webkul_Marketplace_Block_Orders extends Mage_Core_Block_Template
{
    protected $_links = array();

    public function __construct(){
        parent::__construct();

        $paramData = $this->getRequest()->getParams();
        $filterOrderid = '';
        $filterOrderstatus = '';
        $filterDataTo = '';
        $filterDataFrom = '';
        $from = null;
        $to = null;

        if (isset($paramData['s'])) {
            $filterOrderid = $paramData['s'] != '' ? $paramData['s'] : '';
        }
        if (isset($paramData['orderstatus'])) {
            $filterOrderstatus = $paramData['orderstatus'] != '' ? $paramData['orderstatus'] : '';
        }
        if (isset($paramData['from_date'])) {
            $filterDataFrom = $paramData['from_date'] != '' ? $paramData['from_date'] : '';
        }
        if (isset($paramData['to_date'])) {
            $filterDataTo = $paramData['to_date'] != '' ? $paramData['to_date'] : '';
        }

        $customerId = Mage::getSingleton('customer/session')->getId();

        $orderids = $this->getOrderIdsArray($customerId, $filterOrderstatus);

        $ids = $this->getEntityIdsArray($orderids);

        $collection = Mage::getModel('marketplace/saleslist')
        ->getCollection()
        ->addFieldToFilter(
            'autoid',
            array('in' => $ids)
        );

        if ($filterDataTo) {
            $todate = date_create($filterDataTo);
            $to = date_format($todate, 'Y-m-d 23:59:59');
        }
        if ($filterDataFrom) {
            $fromdate = date_create($filterDataFrom);
            $from = date_format($fromdate, 'Y-m-d H:i:s');
        }

        if ($filterOrderid) {
            $collection->addFieldToFilter(
                'magerealorderid',
                array('eq' => $filterOrderid)
            );
        }

        $collection->addFieldToFilter(
            'cleared_at',
            array('datetime' => true, 'from' => $from, 'to' => $to)
        );

        $collection->setOrder(
            'cleared_at',
            'desc'
        );
        
        $this->setCollection($collection);
    }    

    public function getOrderIdsArray($customerId = '', $filterOrderstatus = '')
    {
        $orderids = array();

        $collectionOrders = Mage::getModel('marketplace/saleslist')
        ->getCollection()
        ->addFieldToFilter(
            'mageproownerid',
            $customerId
        )
        ->addFieldToSelect('mageorderid')
        ->distinct(true);

        foreach ($collectionOrders as $collectionOrder) {
            $tracking = Mage::getModel('marketplace/order')->getOrderinfo(
                $collectionOrder->getMageorderid()
            );

            if ($tracking) {
                if ($filterOrderstatus) {
                    if ($tracking->getIsCanceled()) {
                        if ($filterOrderstatus == 'canceled') {
                            array_push($orderids, $collectionOrder->getMageorderid());
                        }
                    } else {
                        $tracking = Mage::getModel('sales/order')->load($collectionOrder->getMageorderid());
                        if ($tracking->getStatus() == $filterOrderstatus) {
                            array_push($orderids, $collectionOrder->getMageorderid());
                        }
                    }
                } else {
                    array_push($orderids, $collectionOrder->getMageorderid());
                }
            }
        }

        return $orderids;
    }

    public function getEntityIdsArray($orderids = array())
    {
        $ids = array();
        foreach ($orderids as $orderid) {
            $collectionIds = Mage::getModel('marketplace/saleslist')
            ->getCollection()
            ->addFieldToFilter(
                'mageorderid',
                $orderid
            )
            ->setOrder('autoid', 'DESC')
            ->setPageSize(1);
            foreach ($collectionIds as $collectionId) {
                $autoid = $collectionId->getAutoid();
                array_push($ids, $autoid);
            }
        }

        return $ids;
    }

    protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $grid_per_page_values = explode(",",Mage::helper('marketplace')->getCatatlogGridPerPageValues());
        $arr_perpage = array();
        foreach ($grid_per_page_values as $value) {
            $arr_perpage[$value] = $value;
        }
        $pager->setAvailableLimit($arr_perpage);
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }    
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    /**
     * Retrieve current order model instance
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return Mage::registry('current_order');
    }

    public function getLinks()
    {
        $this->checkLinks();
        return $this->_links;
    }

    private function checkLinks()
    {
        $order = $this->getOrder();
        $order_id = $order->getId();
        $tracking=Mage::getModel('marketplace/order')->getOrderinfo($order->getId());
        if($tracking!=""){
            $shipmentId = $tracking->getShipmentId();
            $invoiceId=$tracking->getInvoiceId();
            $creditmemo_id=$tracking->getCreditmemoId();
        }
        if (!$order->hasInvoices()) {
            unset($this->_links['invoice']);
        }else{
            if($invoiceId){
                $this->_links['invoice'] = new Varien_Object(array(
                    'name' => 'invoice',
                    'label' => Mage::helper('marketplace')->__('Invoices'),
                    'url' => Mage::getUrl('marketplace/order_invoice/view', array('order_id'=>$order_id,'invoice_id'=>$invoiceId))
                ));
            }
        }
        if (!$order->hasShipments()) {
            unset($this->_links['shipment']);
        }else{
            if($shipmentId){
                $this->_links['shipment'] = new Varien_Object(array(
                    'name' => 'shipment',
                    'label' => Mage::helper('marketplace')->__('Shipments'),
                    'url' => Mage::getUrl('marketplace/order_shipment/view', array('order_id'=>$order_id,'shipment_id'=>$shipmentId))
                ));
            }
        }
        if (!$order->hasCreditmemos()) {
            unset($this->_links['creditmemo']);
        }else{
            if($creditmemo_id){
                $this->_links['creditmemo'] = new Varien_Object(array(
                    'name' => 'creditmemo',
                    'label' => Mage::helper('marketplace')->__('Refunds'),
                    'url' => Mage::getUrl('marketplace/order_creditmemo/viewlist', array('order_id'=>$order_id))
                ));
            }
        }
    }

    protected function _isAllowedAction($action)
    {
        return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/' . $action);
    }
}
