<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

class Webkul_Marketplace_Block_RelatedProducts extends Mage_Core_Block_Template
{
    protected $_productsCollection = null;
    public function __construct(){      
        parent::__construct();

        $sellerId = Mage::getSingleton('customer/session')->getId();

        $productId=$this->getRequest()->getParam('id');

        $sellerCollection = Mage::getModel('marketplace/product')->getCollection();
        $sellerCollection->addFieldToFilter('userid', $sellerId);
        $sellerCollection->addFieldToFilter('mageproductid', array('nin'=>$productId));
        $products=array();
        foreach($sellerCollection as $data){
            array_push($products, $data->getMageproductid());
        }

        $collection = Mage::getModel('catalog/product')->getCollection();
        $collection->addFieldToFilter('entity_id', array('in'=>$products));

        $this->setCollection($collection);
    }
    protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock('page/html_pager', 'custom.pager');
        $grid_per_page_values = explode(",",Mage::helper('marketplace')->getCatatlogGridPerPageValues());
        $arr_perpage = array();
        foreach ($grid_per_page_values as $value) {
            $arr_perpage[$value] = $value;
        }
        $pager->setAvailableLimit($arr_perpage);
        $pager->setCollection($this->getCollection());
        $this->setChild('pager', $pager);
        $this->getCollection()->load();
        return $this;
    }     
    public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }

    public function getRelatedProductData($productId = '')
    {
        $relatedProducts = array();
        if ($productId) {    
            $productColl = Mage::getModel('catalog/product')->load($productId);
            $relatedProductColl = $productColl->getRelatedProducts();

            foreach ($relatedProductColl as $product) {
                $relatedProducts[$product->getId()] = $product->getPosition();
            }
        }
        return $relatedProducts;
    }
    
    public function getAllowedSetsLabel($attributeSetId = '')
    {
        $entityTypeId = Mage::getModel('eav/entity')
                ->setType('catalog_product')
                ->getTypeId();
        $label = '';
        $attributeSetCollection = Mage::getModel('eav/entity_attribute_set')
                        ->getCollection()
                        ->addFieldToFilter('attribute_set_id', $attributeSetId)
                        ->setEntityTypeFilter($entityTypeId);
        foreach($attributeSetCollection as $_attributeSet){
            $label = $_attributeSet->getData('attribute_set_name');
        }
        return $label;
    }
}
