<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
class Webkul_Marketplace_Block_Profile extends Mage_Core_Block_Template
{
	public function _prepareLayout(){
		$partner=$this->getProfileDetail();
		if($partner->getShoptitle()!='') {
            $shop_title = $partner->getShoptitle();
        }
        else {
            $shop_title = $partner->getProfileurl();
        }
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('marketplace')->__("%s Shop",$shop_title));
		$this->getLayout()->getBlock('head')->setKeywords($partner->getMetaKeyword());	
		$this->getLayout()->getBlock('head')->setDescription($partner->getMetaDescription());
		return parent::_prepareLayout();
    }
    
	public function getProfileDetail(){
		$profileurl = Mage::helper('marketplace')->getProfileUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
            $data=Mage::getModel('marketplace/userprofile')->getCollection()
                        ->addFieldToFilter('profileurl',array('eq'=>$profileurl))
                        ->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
                ->addFieldToFilter('profileurl',array('eq'=>$profileurl))
			    ->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ return $seller;}
		}
	}
	
	public function getFeed(){
		$id='';
		$profileurl = Mage::helper('marketplace')->getProfileUrl();
		if($profileurl){
			$storeId = Mage::app()->getStore()->getId();
            $data=Mage::getModel('marketplace/userprofile')->getCollection()
                        ->addFieldToFilter('profileurl',array('eq'=>$profileurl))
                        ->addFieldToFilter('store_id',array('eq'=>$storeId));
            if(!count($data)){
			    $data = Mage::getModel('marketplace/userprofile')->getCollection()
                ->addFieldToFilter('profileurl',array('eq'=>$profileurl))
			    ->addFieldToFilter('store_id', 0);
			}
			foreach($data as $seller){ 
				$id=$seller->getMageuserid();
			}
		}
		if($id){
			return Mage::getModel('marketplace/feedback')->getTotal($id);
		}
	}
	
	public function getBestsellProducts(){
		$products=array();
        $partner = $this->getProfileDetail();
        if(count($partner)){
            $querydata = Mage::getModel('marketplace/product')->getCollection()
								->addFieldToFilter('userid',array('eq'=>$partner->getMageuserid()))
								->addFieldToFilter('status',array('neq'=>2))
                                ->addFieldToSelect('mageproductid')
                                ->setOrder('mageproductid');
            $product = Mage::getModel('catalog/product')->getCollection();
            $product->addAttributeToSelect('*');
            $product->addAttributeToFilter('entity_id', array('in' => $querydata->getData()));
            $product->addAttributeToFilter('visibility', array('in' => array(4) ));
            $product->setPageSize(7)->setCurPage(1);
            foreach ($product as $data1) {
            	array_push($products,$data1->getId());
            }
        }
        return $products;
	}
}