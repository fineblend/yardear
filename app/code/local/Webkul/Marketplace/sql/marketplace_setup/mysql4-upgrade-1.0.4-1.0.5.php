<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) 2010-2016 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 
$installer = $this;
$installer->startSetup();

$installer->run("
	CREATE TABLE IF NOT EXISTS {$this->getTable('marketplace_order_pendingemails')} (
		`entity_id` int(11) unsigned NOT NULL auto_increment,
		`seller_id` int(11) NOT NULL default '0',
		`order_id` int(11) NOT NULL default '0',
		`myvar1` text NOT NULL,
		`myvar2` text NOT NULL,
		`myvar3` text NOT NULL,
		`myvar4` text NOT NULL,
		`myvar5` text NOT NULL,
		`myvar6` text NOT NULL,
		`myvar8` text NOT NULL,
		`myvar9` text NOT NULL,
		`isNotVirtual` text NOT NULL,
		`sender_name` text NOT NULL,
		`sender_email` text NOT NULL,
		`receiver_name` text NOT NULL,
		`receiver_email` text NOT NULL,
		`status` int(11) NOT NULL default '0',
		`created_at` datetime NOT NULL,
		`updated_at` datetime NOT NULL,
		PRIMARY KEY (`entity_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");


$prefix = Mage::getConfig()->getTablePrefix();

$connection = $this->getConnection();
/**
 * Update tables 'sales_flat_order'
 */
$connection->addColumn($prefix.'sales_flat_order', 'order_approval_status', 'int(2) NOT NULL DEFAULT 0');

/**
 * Update tables 'sales_flat_order_grid'
 */
$connection->addColumn($prefix.'sales_flat_order_grid', 'order_approval_status', 'int(2) NOT NULL DEFAULT 0');

/**
 * Update tables 'marketplace_userdata'
 */
$connection->addColumn($prefix.'marketplace_userdata', 'store_id', 'int(11) NOT NULL DEFAULT 0');

$attrCode = 'mp_product_cart_limit';

$objCatalogEavSetup = Mage::getResourceModel('catalog/eav_mysql4_setup', 'core_setup');
$attrIdTest = $objCatalogEavSetup->getAttributeId(Mage_Catalog_Model_Product::ENTITY, $attrCode);

if ($attrIdTest === false) {
    $objCatalogEavSetup->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attrCode, array(
        'type' => 'varchar',
        'backend' => '',
        'frontend' => '',
        'label' => 'Product Purchase Limit for Customer',
        'input' => 'text',
        'class' => '',
        'source' => '',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible' => true,
        'required' => false,
        'user_defined' => false,
        'default' => '',
        'searchable' => false,
        'filterable' => false,
        'comparable' => false,
        'visible_on_front' => false,
        'used_in_product_listing' => false,
        'unique' => false,
        'apply_to'     => 'simple,configurable,bundle',
        'frontend_class'=>'validate-zero-or-greater',
        'note' => 'Not applicable on downloadable and virtual product.'
    ));
}

$installer->endSetup(); 
